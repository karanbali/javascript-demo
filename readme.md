# PubSub Todo App

Simple Todo app using PubSub pattern at its core.

## Authors

* **Karan Bali** - [Bitbucket](https://bitbucket.org/karanbali/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
