$(function(){

var events = (function(){
  var topics = {};
  var hOP = topics.hasOwnProperty;

  return {
    subscribe: function(topic, listener) {
      if(!hOP.call(topics, topic)) topics[topic] = [];
      var index = topics[topic].push(listener) -1;
      return {
        remove: function() {
          delete topics[topic][index];
        }
      };
    },
    publish: function(topic, info) {
      if(!hOP.call(topics, topic)) return;
      topics[topic].forEach(function(item) {
      		item(info != undefined ? info : {});
      });
    }
  };
})();

var todo = [];
var count = 0;


$('#add').click(function(e){
    e.preventDefault();
    let name = $('#name').val();
    let pub = events.publish('namechange',name);
    $('#name').val("");
    //console.log($('#comp').parent().attr('id'))
    //e.preventDefault();
})


$('#place').on('click','.cl',function(e){
    e.preventDefault();
    //console.log(e.target.parentNode.id);
    var id = e.target.parentNode.id;
    var pub = events.publish('comp',id);
    //return false;
    //e.preventDefault();
})

$('#place').on('click','.cn',function(e){
    e.preventDefault();
    console.log(e.target.parentNode.id);
    var id = e.target.parentNode.id;
    var pub = events.publish('cncl',id);
    //return false;
    //e.preventDefault();
})


var subpace = events.subscribe('namechange',function(arg){
    todo[count]=arg;
    console.log(arg);
    console.log(count);
    arg = '<div class="container inline"><div class="row inline" id="'+ count +'"><div class="bd inline">  '+ arg + '</div><button id="comp" class ="cl inline" completed=0 type="button">Mark as Complete</button><button id="cncl" class ="cn inline"  type="button">Cancel Todo</button></div></div>' ;
    $('#place').append(arg);
    count++;
    console.log(todo);
})

var subpac = events.subscribe('comp',function(arg){
    //console.log(arg);
    arg = "#"+arg;
    var completed = $(arg).children('.cl').attr('completed');
    if(completed==0){
      $(arg).children('.bd').css('text-decoration','line-through');
      $(arg).children('.cl').attr('completed',1);
    }
    else{
      $(arg).children('.bd').css('text-decoration','none');
      $(arg).children('.cl').attr('completed',0);
    }
    console.log(completed);
})


var sub = events.subscribe('cncl',function(arg){
    console.log(arg);
    console.log(todo);
    todo[arg]=null;
    arg = "#"+arg;
    $(arg).remove();
  
})

})